import java.util.Scanner;
import java.io.*;

class TugasAkhir {

	public static Scanner sc = new Scanner(System.in);
	public static String choosenMenu = "0";
	public static int countPhone = 0;
	public static int maxData = 10;
	public static int longestWord[] = new int[5];
	public static int longestAllWord = 0;
	public static String databaseFileName = "database.txt";

	public static final String addWhiteSpace(String word, int length){
		return word + String.format("%0" + (length - word.length()) + "d", 0).replace("0", " ");
	}

	public static final void countLongestWord(Smartphone phone[]){
		longestWord[0] = 0;
		longestWord[1] = 0;
		longestWord[2] = 0;
		longestWord[3] = 0;
		longestWord[4] = 0;
		if ("MERK".length() > longestWord[0]) {
			longestWord[0] = "MERK".length();
		}
		if ("SERIES".length() > longestWord[1]) {
			longestWord[1] = "SERIES".length();
		}
		if ("NETWORK".length() > longestWord[2]) {
			longestWord[2] = "NETWORK".length();
		}
		if ("PRICE".length() > longestWord[3]) {
			longestWord[3] = "PRICE".length();
		}
		if ("NO.".length() > longestWord[4]) {
			longestWord[4] = "NO.".length();
		}
		for (int i = 0; i <= countPhone - 1; i++) {
			if (phone[i].merk.length() > longestWord[0]) {
				longestWord[0] = phone[i].merk.length();
			}
			if (phone[i].series.length() > longestWord[1]) {
				longestWord[1] = phone[i].series.length();
			}
			if (phone[i].network.length() > longestWord[2]) {
				longestWord[2] = phone[i].network.length();
			}
			if (String.valueOf(phone[i].price).length() > longestWord[3]) {
				longestWord[3] = String.valueOf(phone[i].price).length();
			}
			if (String.valueOf(i + 1).length() > longestWord[4]) {
				longestWord[4] = String.valueOf(i + 1).length();
			}
		}
		longestAllWord = longestWord[0] + longestWord[1] + longestWord[2] + longestWord[3] + longestWord[4];
	}

	public static final void displayErrorOverLoad(){
		System.out.println("FAILED : Data at Maximum ( " + maxData + " )!!");
		System.out.println("         Please delete data before adding new data!");
	}

	public static final void displayAboutProgram(Smartphone phone[], boolean first){
		System.out.println("//////////////////////////////////////////");
		System.out.println("// @author      Fahrur Rifai            //");
		System.out.println("// @email       mfahrurrifai@gmail.com  //");
		System.out.println("// @copyright   www.elfay.id 2017       //");
		System.out.println("// @version     1.0                     //");
		System.out.println("//////////////////////////////////////////");

		if ( ! first) {
			System.out.println("#####################################################");
			showMenu(phone);
		}
	}

	public static void showDataByIndex(int index, Smartphone phone[]) {
		countLongestWord(phone);
		System.out.println(String.format("%0" + ((longestAllWord + 16 - 4) + 1) + "d", 0).replace("0", "-"));
		System.out.print("| " + addWhiteSpace("MERK", longestWord[0] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("SERIES", longestWord[1] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("NETWORK", longestWord[2] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("PRICE", longestWord[3] + 1) + " |"); 
		System.out.println();
		System.out.println(String.format("%0" + ((longestAllWord + 16 - 4) + 1) + "d", 0).replace("0", "-"));
		System.out.print("| " + addWhiteSpace(phone[index].merk, longestWord[0] + 1) + " ");
		System.out.print("| " + addWhiteSpace(phone[index].series, longestWord[1] + 1) + " ");
		System.out.print("| " + addWhiteSpace(phone[index].network, longestWord[2] + 1) + " ");
		System.out.print("| " + addWhiteSpace(String.valueOf(phone[index].price), longestWord[3] + 1) + " |");
		System.out.println();
		System.out.println(String.format("%0" + ((longestAllWord + 16 - 4) + 1) + "d", 0).replace("0", "-"));
	}

	public static void insertData(Smartphone phone[]) {
		if (countPhone >= maxData) {
			displayErrorOverLoad();
		} else {

			int i = countPhone;
			String choice;
			do {
				if (countPhone >= maxData) {
					System.out.println("#####################################################");
					displayErrorOverLoad();
					break;
				} else {
					Smartphone newPhone = new Smartphone();
					System.out.print("Merk : ");
					sc.nextLine();
					newPhone.merk = sc.nextLine();
					System.out.print("Series : ");
					newPhone.series = sc.nextLine();
					System.out.print("Network : ");
					newPhone.network = sc.next();
					System.out.print("Price : ");
					newPhone.price = sc.nextInt();

					phone[countPhone] = newPhone;

					System.out.print("Want more? (y/n) : ");
					choice = sc.next();

					countPhone++;
				}
			} while(choice.equals("y"));
			updateDataBase(phone);
		}

		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void showMenuShowData(Smartphone phone[]) {
		System.out.println("Choose: ");
		System.out.println("1. Show All Data");
		System.out.println("2. Show Data by Merk");
		System.out.println("3. Total Data by Merk");
		System.out.println("4. Back");
		System.out.print("Your Choice [1-4] : ");

		try {
			choosenMenu = sc.next();
		} catch (java.util.InputMismatchException e) {
			choosenMenu = "0";
		}

		System.out.println("#####################################################");

		switch (choosenMenu) {
			case "1": showAllData(phone); break;
			case "2": showDataByMerk(phone); break;
			case "3": countDataByMerk(phone); break;
			case "4": showMenu(phone); break;
			default: 
				System.out.println("Are you kidding me?");
				showMenuAdd(phone); 
				break;
		}
	}

	public static void showAllData(Smartphone phone[]) {
		countLongestWord(phone);
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		System.out.print("| " + addWhiteSpace("NO.", longestWord[4] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("MERK", longestWord[0] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("SERIES", longestWord[1] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("NETWORK", longestWord[2] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("PRICE", longestWord[3] + 1) + " |"); 
		System.out.println();
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		for (int i = 0; i <= countPhone - 1; i++) {
			System.out.print("| " + addWhiteSpace(String.valueOf(i + 1), longestWord[4] + 1) + " ");
			System.out.print("| " + addWhiteSpace(phone[i].merk, longestWord[0] + 1) + " ");
			System.out.print("| " + addWhiteSpace(phone[i].series, longestWord[1] + 1) + " ");
			System.out.print("| " + addWhiteSpace(phone[i].network, longestWord[2] + 1) + " ");
			System.out.print("| " + addWhiteSpace(String.valueOf(phone[i].price), longestWord[3] + 1) + " | ");
			System.out.println();
		}
		if (countPhone == 0) {
			System.out.print("| " + addWhiteSpace("Empty Data.", ((longestAllWord + 20) - 3)) + " |");
			System.out.println();
		}
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void showDataByMerk(Smartphone phone[]) {
		System.out.print("Enter Keyword (merk): ");
		String keyword = sc.next();
		countLongestWord(phone);
		int countData = 0;
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		System.out.print("| " + addWhiteSpace("NO.", longestWord[4] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("MERK", longestWord[0] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("SERIES", longestWord[1] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("NETWORK", longestWord[2] + 1) + " "); 
		System.out.print("| " + addWhiteSpace("PRICE", longestWord[3] + 1) + " |"); 
		System.out.println();
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		for (int i = 0; i <= countPhone - 1; i++) {
			if (keyword.equalsIgnoreCase(phone[i].merk)) {
				System.out.print("| " + addWhiteSpace(String.valueOf(countData + 1), longestWord[4] + 1) + " ");
				System.out.print("| " + addWhiteSpace(phone[i].merk, longestWord[0] + 1) + " ");
				System.out.print("| " + addWhiteSpace(phone[i].series, longestWord[1] + 1) + " ");
				System.out.print("| " + addWhiteSpace(phone[i].network, longestWord[2] + 1) + " ");
				System.out.print("| " + addWhiteSpace(String.valueOf(phone[i].price), longestWord[3] + 1) + " | ");
				System.out.println();
				countData++;
			}
		}
		if (countData == 0) {
			System.out.print("| " + addWhiteSpace("Data not available.", ((longestAllWord + 20) - 3)) + " |");
			System.out.println();
		}
		System.out.println(String.format("%0" + ((longestAllWord + 20) + 1) + "d", 0).replace("0", "-"));
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void countDataByMerk(Smartphone phone[]) {
		System.out.print("Enter Keyword (merk): ");
		String keyword = sc.next();
		countLongestWord(phone);
		int countData = 0;
		System.out.println(String.format("%0" + ((longestAllWord + 16) + 1) + "d", 0).replace("0", "-"));
		for (int i = 0; i <= countPhone - 1; i++) {
			if (keyword.equalsIgnoreCase(phone[i].merk)) {
				countData++;
			}
		}
		if (countData == 0) {
			System.out.print("| " + addWhiteSpace("Data not available.", ((longestAllWord + 16) - 3)) + " |");
		} else {
			System.out.print("| " + addWhiteSpace("Found " + countData + " data.", ((longestAllWord + 16) - 3)) + " |");
		}
		System.out.println();
		System.out.println(String.format("%0" + ((longestAllWord + 16) + 1) + "d", 0).replace("0", "-"));
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void showMenuAdd(Smartphone phone[]) {
		if (countPhone >= maxData) {
			displayErrorOverLoad();
			System.out.println("#####################################################");
			showMenu(phone);
		} else {
			System.out.println("Choose : ");
			System.out.println("1. Add Data in Front");
			System.out.println("2. Add Data in Middle");
			System.out.println("3. Add Data in Last");
			System.out.println("4. Back");
			System.out.print("Your Choice [1-4] : ");

			try {
				choosenMenu = sc.next();
			} catch (java.util.InputMismatchException e) {
				choosenMenu = "0";
			}

			System.out.println("#####################################################");

			switch (choosenMenu) {
				case "1": addDataAtFront(phone); break;
				case "2": addDataAtMiddle(phone); break;
				case "3": addDataAtLast(phone); break;
				case "4": showMenu(phone); break;
				default: 
					System.out.println("Are you kidding me?");
					showMenuAdd(phone); 
					break;
			}
		}
	}

	public static void addDataAtFront(Smartphone phone[]) {
		Smartphone newPhone = new Smartphone();
		System.out.print("Merk : ");
		sc.nextLine();
		newPhone.merk = sc.nextLine();
		System.out.print("Series : ");
		newPhone.series = sc.nextLine();
		System.out.print("Network : ");
		newPhone.network = sc.next();
		System.out.print("Price : ");
		newPhone.price = sc.nextInt();
		for (int i = countPhone - 1; i >= 0; i--)   { 
			phone[i + 1] = phone[i];
		}
		phone[0] = newPhone;
		countPhone++;
		System.out.println("Success adding data!");
		updateDataBase(phone);

		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void addDataAtMiddle(Smartphone phone[]) {
		Smartphone newPhone = new Smartphone();
		System.out.print("Merk : ");
		sc.nextLine();
		newPhone.merk = sc.nextLine();
		System.out.print("Series : ");
		newPhone.series = sc.nextLine();
		System.out.print("Network : ");
		newPhone.network = sc.next();
		System.out.print("Price : ");
		newPhone.price = sc.nextInt();

		int indexNewPhone;
		System.out.print("Enter position for the new data : ");
		try {
			indexNewPhone = sc.nextInt();
		} catch (java.util.InputMismatchException e) {
			indexNewPhone = 0;
			sc.nextLine(); // clear buff
		}
		
		if (indexNewPhone > 0 && indexNewPhone <= countPhone) {
			for (int i = countPhone - 1; i >= indexNewPhone - 1; i--) {
				phone[i + 1] = phone[i];
			}
			phone[indexNewPhone - 1] = newPhone;
			countPhone++;
			System.out.println("Success adding data!");
			updateDataBase(phone);
		} else {
			System.out.println("Failed adding data!");
		}
		
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void addDataAtLast(Smartphone phone[]) {
		Smartphone newPhone = new Smartphone();
		System.out.print("Merk : ");
		sc.nextLine(); // clear buff
		newPhone.merk = sc.nextLine();
		System.out.print("Series : ");
		newPhone.series = sc.nextLine();
		System.out.print("Network : ");
		newPhone.network = sc.next();
		System.out.print("Price : ");
		newPhone.price = sc.nextInt();
		phone[countPhone] = newPhone;
		countPhone++;
		System.out.println("Success adding data!");

		updateDataBase(phone);
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void searchData(Smartphone phone[]) {
		String keyword;
		int foundIndex = -1;
		boolean dataFound = false;
		System.out.print("Enter Keyword (merk): ");
		keyword = sc.next();
		for (int i = 0; i <= countPhone - 1; i++) {
			if (phone[i].merk.equalsIgnoreCase(keyword)) {
				dataFound = true;
				foundIndex = i;
				break;
			}
		}
		if (dataFound == true) {
			System.out.println("Found at index : " + foundIndex);
			showDataByIndex(foundIndex, phone);
		} else {
			System.out.println("Sory, your keyword was not found in our database.");
		}
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void editData(Smartphone phone[]) {
		int indexEditPhone;
		System.out.print("Enter position (not index) to edit : ");
		indexEditPhone = sc.nextInt();
		indexEditPhone = indexEditPhone - 1;

		if (indexEditPhone >= 0) {
			System.out.print("New Merk (old: " + phone[indexEditPhone].merk + ") : ");
			sc.nextLine(); // clear buff
			phone[indexEditPhone].merk = sc.nextLine();
			System.out.print("New Series (old: " + phone[indexEditPhone].series + ") : ");
			phone[indexEditPhone].series = sc.nextLine();
			System.out.print("New Network (old: " + phone[indexEditPhone].network + ") : ");
			phone[indexEditPhone].network = sc.next();
			System.out.print("New Price (old: " + phone[indexEditPhone].price + ") : ");
			phone[indexEditPhone].price = sc.nextInt();

			System.out.println("Edit data success!");
			updateDataBase(phone);
		} else {
			System.out.println("Edit data Failed (Data not found)!");
		}
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void deleteData(Smartphone phone[]) {
		int indexDeletePhone;
		System.out.print("Enter position (not index) to delete : ");
		indexDeletePhone = sc.nextInt();
		if (indexDeletePhone > 0) {
			for (int i = indexDeletePhone - 1; i < countPhone - 1; i++) {
				phone[i] = phone[i + 1];
			}
			countPhone--;
			System.out.println("Success delete data!");
			updateDataBase(phone);
		} else {
			System.out.println("Failed delete data! (Data not found)");
		}
		System.out.println("#####################################################");
		showMenu(phone);
	}

	public static void showMenuSort(Smartphone phone[]) {
		System.out.println("Choose : ");
		System.out.println("1. Sort By Merk (ASC)");
		System.out.println("2. Sort By Merk (DESC)");
		System.out.println("3. Sort By Series (ASC)");
		System.out.println("4. Sort By Series (DESC)");
		System.out.println("5. Sort By Network (ASC)");
		System.out.println("6. Sort By Network (DESC)");
		System.out.println("7. Sort By Price (ASC)");
		System.out.println("8. Sort By Price (DESC)");
		System.out.println("9. Back");
		System.out.print("Your Choice [1-9] : ");

		try {
			choosenMenu = sc.next();
		} catch (java.util.InputMismatchException e) {
			choosenMenu = "0";
		}

		System.out.println("#####################################################");

		switch (choosenMenu) {
			case "1": sortData(phone, "merk", "asc"); break;
			case "2": sortData(phone, "merk", "desc"); break;
			case "3": sortData(phone, "series", "asc"); break;
			case "4": sortData(phone, "series", "desc"); break;
			case "5": sortData(phone, "network", "asc"); break;
			case "6": sortData(phone, "network", "desc"); break;
			case "7": sortData(phone, "price", "asc"); break;
			case "8": sortData(phone, "price", "desc"); break;
			case "9": showMenu(phone); break;
			default: 
				System.out.println("Are you kidding me?");
				showMenuAdd(phone); 
				break;
		}
	}

	public static void sortData(Smartphone phone[], String sortBy, String sortDir) {
		Smartphone tempPhone = new Smartphone();
		int lastIndex = countPhone - 1;
		for (int j = 0; j <= lastIndex - 1; j++) {
			for (int i = 0; i <= (lastIndex - 1) - j; i++) {
				if (sortBy == "merk") {
					if (sortDir.equalsIgnoreCase("asc")) {
						if (phone[i].merk.compareTo(phone[i + 1].merk) > 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					} else {
						if (phone[i].merk.compareTo(phone[i + 1].merk) < 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					}
				} else if (sortBy == "series") {
					if (sortDir.equalsIgnoreCase("asc")) {
						if (phone[i].series.compareTo(phone[i + 1].series) > 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					} else {
						if (phone[i].series.compareTo(phone[i + 1].series) < 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					}
				} else if (sortBy == "network") {
					if (sortDir.equalsIgnoreCase("asc")) {
						if (phone[i].network.compareTo(phone[i + 1].network) > 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					} else {
						if (phone[i].network.compareTo(phone[i + 1].network) < 0) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					}
				} else if (sortBy == "price") {
					if (sortDir.equalsIgnoreCase("asc")) {
						if (phone[i].price > phone[i + 1].price) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					} else {
						if (phone[i].price < phone[i + 1].price) {
							tempPhone = phone[i];
							phone[i] = phone[i + 1];
							phone[i + 1] = tempPhone;
						}
					}
				}
			}
		}
		updateDataBase(phone);
		showAllData(phone);
	}

	public static void showMenu(Smartphone phone[]) {
		System.out.println("What do you want to do?");
		System.out.println("1. Insert Data");
		System.out.println("2. Show Data");
		System.out.println("3. Add Data");
		System.out.println("4. Search Data");
		System.out.println("5. Edit Data");
		System.out.println("6. Delete Data");
		System.out.println("7. Sort Data");
		System.out.println("8. About Program");
		System.out.println("9. Exit");
		System.out.print("Your Choice [1-9] : ");

		try {
			choosenMenu = sc.next();
		} catch (java.util.InputMismatchException e) {
			choosenMenu = "0";
		}
		
		System.out.println("#####################################################");

		switch (choosenMenu) {
			case "1": insertData(phone); break;
			case "2": showMenuShowData(phone); break;
			case "3": showMenuAdd(phone); break;
			case "4": searchData(phone); break;
			case "5": editData(phone); break;
			case "6": deleteData(phone); break;
			case "7": showMenuSort(phone); break;
			case "8": displayAboutProgram(phone, false); break;
			case "9": 
				System.out.println("Goodby!");
				System.exit(0); 
				break;
			default: 
				System.out.println("Are you kidding me?");
				showMenu(phone); 
				break;
		}
	}

	public static final void initialize(Smartphone phone[]){
		for (int i = 0; i < phone.length - 1; i++) {
			phone[i] = new Smartphone();
		}

		String line;
		int i = 0;
		try {
			BufferedReader fileInput = new BufferedReader(new FileReader(
				new File(databaseFileName)));
			do {
				line = fileInput.readLine();
				if (line != null) {
					String[] data = line.split(",");
					phone[i].merk = data[0];
					phone[i].series = data[1];
					phone[i].network = data[2];
					phone[i].price = Integer.valueOf(data[3]);
					countPhone++;
					i++;
				}
			} while (line != null);
			fileInput.close();

		} catch (EOFException eofe) {
			System.out.println("No more line to read");
			System.exit(0);
		} catch (IOException ioe) {
			System.out.println(ioe);
			System.out.println("Error reading a file");
			System.exit(0);
		}

	}

	public static void updateDataBase(Smartphone phone[]) {
		FileOutputStream fout = null;
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(databaseFileName);
			pw.close();
		} catch (IOException e) {
			System.out.println("Something Wrong!");
			System.exit(0);
		}
		try {
			fout = new FileOutputStream(databaseFileName, true);
			for (int i = 0; i <= countPhone - 1; i++) {
				fout.write(phone[i].to_string().getBytes());
			}
		} catch (FileNotFoundException e) {
			System.out.println("File " + databaseFileName + " Not Found.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Something Wrong!");
			System.exit(0);
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch (IOException e) {
					System.out.println("Something Wrong!");
					System.exit(0);
				}
			}
		}
	}

	public static void main(String[] args) {
		
		Smartphone phone[] = new Smartphone[maxData];
		initialize(phone);

		// show welcome message
		System.out.println("Welcome to My Program !!");
		displayAboutProgram(phone, true);

		// Go
		showMenu(phone);
	}
}

class Smartphone {
	String merk, series, network;
	int price;

	public String to_string(){
		return merk + "," + series + "," + network + "," + price + "\n";
	}
}